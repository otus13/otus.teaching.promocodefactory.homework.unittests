﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost
{
    public class TestFixture : IDisposable
    {
        public IServiceProvider ServiceProvider { get; }
        public DataContext Context { get; }

        /// <summary>
        /// Выполняется перед запуском тестов
        /// </summary>
        public TestFixture()
        {
            var builder = new ConfigurationBuilder();
            var configuration = builder.Build();
            var serviceCollection = Configuration.GetServiceCollection(configuration, "Tests");
            var serviceProvider = serviceCollection
                .ConfigureInMemoryContext()
                .BuildServiceProvider();

            ServiceProvider = serviceProvider;

            Context = ServiceProvider.GetService<DataContext>();

            SeedData();
        }

        private void SeedData()
        {
            Context.Database.EnsureCreated();

            Context.Partners.Add(new Partner()
            {
                Id = Guid.NewGuid(),
                Name = "Мебель",
                IsActive = true,
                NumberIssuedPromoCodes = 5,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                    {
                        new PartnerPromoCodeLimit()
                        {
                            Id = Guid.NewGuid(),
                            CreateDate = new DateTime(2020, 07, 9),
                            EndDate = new DateTime(2020, 10, 9),
                            CancelDate = null,
                            Limit = 50
                        }
                    }
            });

            Context.SaveChanges(true);
        }

        public void Dispose()
        {
        }
    }
}
