﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepository;
        private readonly PartnersController _partnersController;
        private readonly IFixture _fixture;

        private SetPartnerPromoCodeLimitRequest SetPartnerPromoCodeLimitRequest => _fixture
            .Create<SetPartnerPromoCodeLimitRequest>();

        private Partner CreatePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.NewGuid(),
                Name = "Одежда",
                IsActive = true,
                NumberIssuedPromoCodes = 10,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.NewGuid(),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepository = _fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = _fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        /// <summary>
        /// 1. Если партнер не найден, то также нужно выдать ошибку 404;
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_WhenPartnerNotFound_ReturnsNotFoundResult()
        {
            _partnersRepository.Setup(r => r.GetByIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult((Partner)null));

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(new Guid(),
                SetPartnerPromoCodeLimitRequest);

            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        ///2. Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_WhenPartnerIsNotActive_ReturnsBadRequestObjectResult()
        {
            var partner = CreatePartner();
            partner.IsActive = false;

            _partnersRepository.Setup(r => r.GetByIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(partner));

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(new Guid(),
                SetPartnerPromoCodeLimitRequest);

            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        ///3.1 Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitRequest_WhenSetLimit_SetsPromoCodeCountToZero()
        {
            var partner = CreatePartner();

            _partnersRepository.Setup(r => r.GetByIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(partner));

            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id,
                SetPartnerPromoCodeLimitRequest);

            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        ///3.2 если лимит закончился, то количество не обнуляется;
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_WhenLimitExceeded_PersistsPromoCodeCount()
        {
            var partner = CreatePartner();
            partner.PartnerLimits.Single().CancelDate = DateTime.Now;

            _partnersRepository.Setup(r => r.GetByIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(partner));

            var oldNumberIssuedPromoCodes = partner.NumberIssuedPromoCodes;

            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id,
                SetPartnerPromoCodeLimitRequest);

            partner.NumberIssuedPromoCodes.Should().Be(oldNumberIssuedPromoCodes);
        }

        /// <summary>
        ///4. При установке лимита нужно отключить предыдущий лимит;
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_WhenSetNewLimit_SetsEndDateOfPreviousLimit()
        {
            var partner = CreatePartner();
            var limit = partner.PartnerLimits.Single();

            _partnersRepository.Setup(r => r.GetByIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(partner));

            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id,
                SetPartnerPromoCodeLimitRequest);

            limit.CancelDate.Should().NotBeNull();
        }

        /// <summary>
        ///5. Лимит должен быть больше 0;
        /// </summary>
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task SetPartnerPromoCodeLimitAsync_WhenLimitLessOrEqualsZero_ThrowsInvalidArgument(int limit)
        {
            var partner = CreatePartner();
            var setPartnerPromoCodeLimitRequest = _fixture
                .Build<SetPartnerPromoCodeLimitRequest>()
                .With(s => s.Limit, limit)
                .Create();
            _partnersRepository.Setup(r => r.GetByIdAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(partner));

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id,
                setPartnerPromoCodeLimitRequest);

            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
    }
}