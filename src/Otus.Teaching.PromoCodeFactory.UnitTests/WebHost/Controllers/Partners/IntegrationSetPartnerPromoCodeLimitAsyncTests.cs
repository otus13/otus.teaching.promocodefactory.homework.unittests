﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class IntegrationSetPartnerPromoCodeLimitAsyncTests : IClassFixture<TestFixture>
    {
        private readonly DataContext _context;
        private readonly PartnersController _partnersController;
        private readonly SetPartnerPromoCodeLimitRequest _setPartnerPromoCodeLimitRequest;

        public IntegrationSetPartnerPromoCodeLimitAsyncTests(TestFixture testFixture)
        {
            var partnersRepository = testFixture.ServiceProvider.GetRequiredService<IRepository<Partner>>();

            _partnersController = new PartnersController(partnersRepository);
            _context = testFixture.Context;
            _setPartnerPromoCodeLimitRequest = new Fixture()
                    .Customize(new AutoMoqCustomization())
                    .Create<SetPartnerPromoCodeLimitRequest>();
        }

        /// <summary>
        ///6. Нужно убедиться, что сохранили новый лимит в базу данных (это нужно проверить Unit-тестом);
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_SavesLimitInDatabase()
        {
            // Arrange
            var partnersCount = _context.Partners.Count();
            var partnerId = _context.Partners.First().Id;

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, _setPartnerPromoCodeLimitRequest);

            // Assert
            var partner = _context.Partners.First(p => p.Id == partnerId);
            var partnerLimit = partner.PartnerLimits.Single(l => !l.CancelDate.HasValue);

            partner.PartnerLimits.Should().HaveCount(++partnersCount);
            partnerLimit.Limit.Should().Be(_setPartnerPromoCodeLimitRequest.Limit);
        }
    }
}